from django.shortcuts import render
from todos.models import TodoItem, TodoList
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "list_todos"


class TodoDetailView(DetailView):
    model = TodoItem
    template_name = "todos/detail.view"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("list_todos")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/createitem.html"
    fields = ["task"]


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/updateitem.html"
    fields = ["task", "due date", "is_completed checkbox", "select list"]
